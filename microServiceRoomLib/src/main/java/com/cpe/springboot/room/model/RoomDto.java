package com.cpe.springboot.room.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class RoomDto {

	public RoomDto() {
		this.id = 0;
		this.player1 = 0;
		this.player2 = 0;
		this.cardPlayer1 = 0;
		this.cardPlayer2 = 0;
		this.bet = 0;
		this.name = "";
	}
	
	public RoomDto(Integer id, Integer player1, Integer player2, Integer cardPlayer1, Integer cardPlayer2, Integer bet, String name) {
		super();
		this.id = id;
		this.player1 = player1;
		this.player2 = player2;
		this.cardPlayer1 = cardPlayer1;
		this.cardPlayer2 = cardPlayer2 ;
		this.bet = bet;
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer player1;
	private Integer player2;
	private Integer cardPlayer1;
	private Integer cardPlayer2;
	private Integer bet;
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getplayer1() {
		return player1;
	}

	public void setplayer1(Integer player1) {
		this.player1 = player1;
	}
	
	public Integer getplayer2() {
		return player2;
	}

	public void setplayer2(Integer player2) {
		this.player2 = player2;
	}

	public Integer getCardPlayer1() {
		return cardPlayer1;
	}

	public void setCardPlayer1(Integer cardPlayer1) {
		this.cardPlayer1 = cardPlayer1;
	}

	public Integer getCardPlayer2() {
		return cardPlayer2;
	}

	public void setCardPlayer2(Integer cardPlayer2) {
		this.cardPlayer2 = cardPlayer2;
	}

	public Integer getBet() {
		return bet;
	}

	public void setBet(Integer bet) {
		this.bet = bet;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
