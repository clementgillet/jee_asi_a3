package com.cpe.springboot.room.WSInterface;

import java.net.URI;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class WSInterfaceRoom {

	public void removeRoom(Integer idRoom) {
		final String url = "http://localhost:8084/rooms/" + idRoom;
		try {
			URI uri = new URI(url);
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.delete(uri);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
