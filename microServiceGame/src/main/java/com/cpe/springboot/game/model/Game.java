package com.cpe.springboot.game.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Game {
	

	public Game() {
		this.id = 0;
		this.player1 = 0;
		this.player2 = 0;
		this.cardPlayer1 = 0;
		this.cardPlayer2 = 0;
		this.bet = 0;
		this.roomId = 0;
	}
	
	public Game(Integer id, Integer player1, Integer player2, Integer cardPlayer1, Integer cardPlayer2, Integer bet, Integer roomId) throws GameException {
		super();
		if (cardPlayer1 == cardPlayer2) {
			throw new GameException("Même carte pour les deux joueurs");
		}
		
		this.id = id;
		this.player1 = player1;
		this.player2 = player2;
		this.cardPlayer1 = cardPlayer1;
		this.cardPlayer2 = cardPlayer2;
		this.bet = bet;
		this.roomId = roomId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer player1;
	private Integer player2;
	private Integer cardPlayer1;
	private Integer cardPlayer2;
	private Integer bet;
	private Integer roomId;

	public Integer getPlayer1() {
		return player1;
	}

	public void setPlayer1(Integer player1) {
		this.player1 = player1;
	}

	public Integer getPlayer2() {
		return player2;
	}

	public void setPlayer2(Integer player2) {
		this.player2 = player2;
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getplayer1() {
		return player1;
	}

	public void setplayer1(Integer player1) {
		this.player1 = player1;
	}
	
	public Integer getplayer2() {
		return player2;
	}

	public void setplayer2(Integer player2) {
		this.player2 = player2;
	}

	public Integer getCardPlayer1() {
		return cardPlayer1;
	}

	public void setCardPlayer1(Integer cardPlayer1) {
		this.cardPlayer1 = cardPlayer1;
	}

	public Integer getCardPlayer2() {
		return cardPlayer2;
	}

	public void setCardPlayer2(Integer cardPlayer2) {
		this.cardPlayer2 = cardPlayer2;
	}

	public Integer getBet() {
		return bet;
	}

	public void setBet(Integer bet) {
		this.bet = bet;
	}

}
