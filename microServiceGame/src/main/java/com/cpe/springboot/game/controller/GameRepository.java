package com.cpe.springboot.game.controller;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.game.model.Game;

public interface GameRepository extends CrudRepository<Game, Integer> {
	
	public Game findById(Integer id);
	
	public Game findByRoomId(Integer id);

}
