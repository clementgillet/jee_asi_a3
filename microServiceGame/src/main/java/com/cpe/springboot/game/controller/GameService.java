package com.cpe.springboot.game.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.WSInterface.WSInterfaceCard;
import com.cpe.springboot.card.model.CardDto;
import com.cpe.springboot.game.model.Game;
import com.cpe.springboot.room.WSInterface.WSInterfaceRoom;
import com.cpe.springboot.user.WSInterface.WSInterfaceUsers;
import com.cpe.springboot.user.model.UserDto;

@Service
public class GameService {
	
	@Autowired
	WSInterfaceCard wsInterfaceCard ;
	
	@Autowired
	WSInterfaceUsers wsInterfaceUsers;
	
	@Autowired
	WSInterfaceRoom wsInterfaceRoom;

	@Autowired
	private GameRepository gameRepository;

	public List<Game> getAllGames() {
		List<Game> games = new ArrayList<>();
		gameRepository.findAll().forEach(games::add);
		return games;
	}

	public Game getGame(Integer id) {
		return gameRepository.findById(id);
	}

	public Game addGame(Game game, Integer idRoom) {
		//On supprime la room associée
		this.wsInterfaceRoom.removeRoom(idRoom);
		return gameRepository.save(game);
	}

	public void updateGame(Game game) {
		gameRepository.save(game);

	}

	public void deleteGame(String id) {
		gameRepository.delete(Integer.valueOf(id));
	}
	public int isPlayer1()
	{
		return ((int)((Math.random()*10)%2)) ;
	}

	public Integer playGame(Integer id) {
		Game game = gameRepository.findOne(id);
		
		CardDto cardPlayer1 = wsInterfaceCard.getCard(game.getCardPlayer1());
		CardDto cardPlayer2 = wsInterfaceCard.getCard(game.getCardPlayer2());
		List<CardDto> cardsPlayer = new ArrayList<CardDto>();
		cardsPlayer.add(cardPlayer1);
		cardsPlayer.add(cardPlayer2);
		
		int tourPlayer = isPlayer1();
		
		while(cardPlayer1.getHp() > 0 && cardPlayer2.getHp() > 0) {
			cardsPlayer.get(tourPlayer).setHp(
					cardsPlayer.get(tourPlayer).getHp() - 
					cardsPlayer.get((tourPlayer+1)%2).getAttack());
		}
		UserDto userDtoWinner;

		UserDto userDtoLooser;

		//on donne l'argent au gagnant
		if(cardPlayer1.getHp() <= 0) {
			
			userDtoWinner = wsInterfaceUsers.getUser(game.getplayer2());
			userDtoLooser = wsInterfaceUsers.getUser(game.getplayer1());
			userDtoWinner.setMoney(userDtoWinner.getMoney() + game.getBet());
			userDtoLooser.setMoney(userDtoLooser.getMoney() - game.getBet());
			wsInterfaceUsers.updateUser(userDtoWinner);
			wsInterfaceUsers.updateUser(userDtoLooser);

		} else {
			userDtoWinner = wsInterfaceUsers.getUser(game.getplayer1());
			userDtoLooser = wsInterfaceUsers.getUser(game.getplayer2());
			userDtoWinner.setMoney(userDtoWinner.getMoney() + game.getBet());

			userDtoLooser.setMoney(userDtoLooser.getMoney() - game.getBet());
			wsInterfaceUsers.updateUser(userDtoWinner);
			wsInterfaceUsers.updateUser(userDtoLooser);

		}
		//On retire l'energy
		cardPlayer1.setEnergy(cardPlayer1.getEnergy() - 20);
		cardPlayer2.setEnergy(cardPlayer2.getEnergy() - 20);
		//Puis on met à jour les cartes
		wsInterfaceCard.updateCard(cardPlayer1);
		wsInterfaceCard.updateCard(cardPlayer2);
		return userDtoWinner.getId();
	}
	
	public Integer getGameIdByRoomId(Integer id) {
		return this.gameRepository.findByRoomId(id).getId();
	}

}
