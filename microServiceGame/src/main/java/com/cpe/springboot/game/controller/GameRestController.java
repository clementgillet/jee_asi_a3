package com.cpe.springboot.game.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.game.controller.GameService;
import com.cpe.springboot.game.model.Game;
import com.cpe.springboot.game.model.GameException;
import com.cpe.springboot.room.model.RoomDto;

@RestController
public class GameRestController {

	@Autowired
	private GameService gameService;

	@RequestMapping("/games")
	private List<Game> getAllGames() {
		System.out.println("LOG GameRestController : getAllGames() - GET /games");
		return gameService.getAllGames();
	}

	@RequestMapping("/games/{id}")
	private Game getGame(@PathVariable Integer id) {
		System.out.println("LOG GameRestController : getGame() - GET /games/" + id);
		return gameService.getGame(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/games")
	public Integer addGame(@RequestBody RoomDto roomDto) {
		System.out.println("LOG GameRestController : addGame() - POST /games");
		try {
			if (roomDto.getCardPlayer1() != 0 && roomDto.getCardPlayer2() != 0) {
				Game game = new Game(null, roomDto.getplayer1(), roomDto.getplayer2(), roomDto.getCardPlayer1(),
						roomDto.getCardPlayer2(), roomDto.getBet(), roomDto.getId());
				gameService.addGame(game, roomDto.getId());
				return game.getId();
			}
		} catch (GameException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/games")
	public void updateGame(@RequestBody Game game) {
		System.out.println("LOG GameRestController : updateGame() - PUT /games");
		gameService.updateGame(game);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/games/{id}")
	public void deleteGame(@PathVariable String id) {
		System.out.println("LOG GameRestController : deleteGame() - DELETE /games/" + id);
		gameService.deleteGame(id);
	}

	@RequestMapping("/games/play/{id}")
	private Integer playGame(@PathVariable Integer id) {
		System.out.println("LOG GameRestController : playGame() - GET /games/play/" + id);
		return gameService.playGame(id);
	}

	@RequestMapping("/games/getGameByRoom/{id}")
	private Integer getGameIdByRoomId(@PathVariable Integer id) {
		System.out.println("LOG GameRestController : getGameIdByRoomId() - /games/getGameByRoom/" + id);
		return gameService.getGameIdByRoomId(id);
	}

}
