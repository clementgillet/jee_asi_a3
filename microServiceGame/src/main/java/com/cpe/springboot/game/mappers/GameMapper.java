package com.cpe.springboot.game.mappers;

import org.springframework.beans.BeanUtils;

import com.cpe.springboot.game.dto.GameDto;
import com.cpe.springboot.game.model.Game;

public class GameMapper {
	
	public static GameDto map(Game game) {
		GameDto rDto = new GameDto();
		BeanUtils.copyProperties(game, rDto);
		return rDto;
	}
}
