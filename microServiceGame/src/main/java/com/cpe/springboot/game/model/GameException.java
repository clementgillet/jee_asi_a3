package com.cpe.springboot.game.model;

public class GameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 214684321564322L;

	public GameException(String desc) {
		super(desc);
	}

}
