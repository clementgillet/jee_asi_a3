package com.cpe.springboot;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cpe.springboot.card.model.CardDto;
import com.cpe.springboot.card.WSInterface.WSInterfaceCard;
import com.cpe.springboot.game.controller.GameRepository;
import com.cpe.springboot.game.controller.GameService;
import com.cpe.springboot.game.model.Game;
import com.cpe.springboot.game.model.GameException;
import com.cpe.springboot.user.WSInterface.WSInterfaceUsers;
import com.cpe.springboot.user.model.UserDto;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RestJpaMysqlSpringBootSampleApplicationTests {
	@Mock
	@Autowired
	GameRepository gameRepository;
	
	@Mock
	WSInterfaceCard wSInterfaceCard ;
	
	@Mock
	WSInterfaceUsers wSInterfaceUsers;
	
	
	@InjectMocks
	GameService gameService;
	
	//@Test
	//public void isPlayer1Test() {
		//GameService gameService = new GameService();

		//Assert.assertEquals(1, gameService.isPlayer1());

	//}
	
	
	@Test
	public void playGamecheckHPTest() throws GameException
	{
		
		Integer id = 1;
		//CardDto cardPlayer1 = Mockito.mock(CardDto.class);
		//CardDto cardPlayer2 = Mockito.mock(CardDto.class);
		CardDto cardPlayer1 = new CardDto(1, "family1", "image1", "nom1", "description1", 100,
				100, 30, 10, "imgurl1");
		
		CardDto cardPlayer2 = new CardDto(2, "family2", "image2", "nom2", "description2", 100,
				200, 30, 10, "imgurl2");
		UserDto player1 =new UserDto(1, "player1", "player1", 100);
		UserDto player2 =new UserDto(1, "player2", "player2", 100);
		
		//WSInterfaceCard wSInterfaceCard = Mockito.mock((WSInterfaceCard.class));
		
		
		Game game = new Game(1, 1, 2, 3, 4, 5, 0);
		
		Mockito.when(gameRepository.findOne(Mockito.anyInt())).thenReturn(game);
		//Mockito.when(gameService.isPlayer1()).thenReturn(1);
		
		gameService.isPlayer1();
		
		Mockito.when(wSInterfaceCard.getCard(game.getCardPlayer1())).thenReturn(cardPlayer1);
		Mockito.when(wSInterfaceCard.getCard(game.getCardPlayer2())).thenReturn(cardPlayer2);
		Mockito.when(wSInterfaceUsers.getUser(game.getplayer2())).thenReturn(player2);
		Mockito.when(wSInterfaceUsers.getUser(game.getplayer1())).thenReturn(player1);
		
		gameService.playGame(id);
		int Hpf = -20;
		//System.out.println("HP : " + cardPlayer1.getHp());
		//System.out.println("HP : " + cardPlayer2.getHp());
		if (cardPlayer1.getHp()<0)
		{
			Assert.assertEquals(Hpf, (int)cardPlayer1.getHp());
		}
		else
		{
			Assert.assertEquals(Hpf, (int)cardPlayer2.getHp());
			
		}
		
		
	}
	
	
	
	@Test
	public void playGamecheckEnergyTest() throws GameException
	{
		
		Integer id = 1;

		//CardDto cardPlayer1 = Mockito.mock(CardDto.class);
		//CardDto cardPlayer2 = Mockito.mock(CardDto.class);
		CardDto cardPlayer1 = new CardDto(1, "family1", "image1", "nom1", "description1", 100,
				100, 30, 10, "imgurl1");
		
		CardDto cardPlayer2 = new CardDto(2, "family2", "image2", "nom2", "description2", 100,
				200, 30, 10, "imgurl2");
		UserDto player1 =new UserDto(1, "player1", "player1", 100);
		UserDto player2 =new UserDto(1, "player2", "player2", 100);
		
		//WSInterfaceCard wSInterfaceCard = Mockito.mock((WSInterfaceCard.class));
		
		
		Game game = new Game(1, 1, 2, 3, 4, 5, 0);
		
		Mockito.when(gameRepository.findOne(Mockito.anyInt())).thenReturn(game);
		//Mockito.when(gameService.isPlayer1()).thenReturn(1);
		
		gameService.isPlayer1();
		
		Mockito.when(wSInterfaceCard.getCard(game.getCardPlayer1())).thenReturn(cardPlayer1);
		Mockito.when(wSInterfaceCard.getCard(game.getCardPlayer2())).thenReturn(cardPlayer2);
		Mockito.when(wSInterfaceUsers.getUser(game.getplayer2())).thenReturn(player2);
		Mockito.when(wSInterfaceUsers.getUser(game.getplayer1())).thenReturn(player1);
		
		gameService.playGame(id);
	
	//	System.out.println("Energy : " + cardPlayer1.getEnergy());
	//	System.out.println("Energy : " + cardPlayer2.getEnergy());
		
		Assert.assertEquals(80, (int)cardPlayer1.getEnergy());		
		
		Assert.assertEquals(180, (int)cardPlayer2.getEnergy());
	
	}
	
	@Test
	public void playGamecheckMoneyTest() throws GameException
	{

		Integer id = 1;
		//CardDto cardPlayer1 = Mockito.mock(CardDto.class);
		//CardDto cardPlayer2 = Mockito.mock(CardDto.class);
		CardDto cardPlayer1 = new CardDto(1, "family1", "image1", "nom1", "description1", 100,
				100, 30, 10, "imgurl1");
		
		CardDto cardPlayer2 = new CardDto(2, "family2", "image2", "nom2", "description2", 100,
				200, 30, 10, "imgurl2");
		UserDto player1 =new UserDto(1, "player1", "player1", 100);
		UserDto player2 =new UserDto(1, "player2", "player2", 100);
		
		//WSInterfaceCard wSInterfaceCard = Mockito.mock((WSInterfaceCard.class));
		
		
		Game game = new Game(1, 1, 2, 3, 4, 5, 0);
		
		Mockito.when(gameRepository.findOne(Mockito.anyInt())).thenReturn(game);
		
		Mockito.when(wSInterfaceCard.getCard(game.getCardPlayer1())).thenReturn(cardPlayer1);
		Mockito.when(wSInterfaceCard.getCard(game.getCardPlayer2())).thenReturn(cardPlayer2);
		Mockito.when(wSInterfaceUsers.getUser(game.getplayer2())).thenReturn(player2);
		Mockito.when(wSInterfaceUsers.getUser(game.getplayer1())).thenReturn(player1);
		
		gameService.playGame(id);
		
		//System.out.println("Money : " + player1.getMoney());
		//System.out.println("Money : " + player2.getMoney());
		if (cardPlayer1.getHp()<0)
		{
			Assert.assertEquals(95, (int)player1.getMoney());
			Assert.assertEquals(105, (int)player2.getMoney());
		}
		else
		{
			Assert.assertEquals(105, (int)player1.getMoney());
			Assert.assertEquals(95, (int)player2.getMoney());
			
		}
		
		
	}
}
