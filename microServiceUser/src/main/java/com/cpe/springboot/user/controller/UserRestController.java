package com.cpe.springboot.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.user.model.RequestUser;
import com.cpe.springboot.user.model.User;

@RestController
public class UserRestController {

	@Autowired
	private UserService userService;

	@RequestMapping("/users")
	private List<User> getAllUsers() {
		System.out.println("LOG UserRestController : getAllUsers() - GET /users");
		return userService.getAllUsers();
	}

	@RequestMapping("/users/{id}")
	private User getUser(@PathVariable Integer id) {
		System.out.println("LOG UserRestController : getUser() - GET /users/" + id);
		return userService.getUser(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/conn")
	private User getUser(@RequestBody RequestUser requestUser) {
		System.out.println("LOG UserRestController : getUser() - POST /conn");
		return userService.findByLoginAndPasswd(requestUser);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/register")
	public void addUser(@RequestBody RequestUser requestUser) {
		System.out.println("LOG UserRestController : addUser() - POST /register");
		userService.addUser(requestUser);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/users")
	public void updateUser(@RequestBody User user) {
		System.out.println("LOG UserRestController : updateUser() - PUT /users");
		userService.updateUser(user);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/users/money/{id}/{money}")
	public void updateMoneyUser(@PathVariable Integer id, @PathVariable Integer money) {
		System.out.println("LOG UserRestController : updateMoneyUser() - PUT /users/money/" + id + "/" + money);
		User u = userService.getUser(id);
		u.setMoney(money);
		userService.updateUser(u);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}")
	public void deleteUser(@PathVariable String id) {
		System.out.println("LOG UserRestController : deleteUser() - DELETE /users/" + id);
		userService.deleteUser(id);
	}

}
