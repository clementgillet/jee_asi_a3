package com.cpe.springboot.user.mappers;

import org.springframework.beans.BeanUtils;

import com.cpe.springboot.user.model.User;
import com.cpe.springboot.user.model.UserDto;

public class UserMapper {

	public static UserDto map(User user) {
		UserDto uDto = new UserDto();
		BeanUtils.copyProperties(user, uDto);
		return uDto;
	}

	public static User map(UserDto uDto) {
		User u = new User();
		BeanUtils.copyProperties(uDto, u);
		return u;
	}
}
