package com.cpe.springboot.user.controller;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.user.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	
	public User findByLoginAndPasswd(String login, String passwd);
}
