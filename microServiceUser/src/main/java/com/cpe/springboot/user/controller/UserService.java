package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.user.model.RequestUser;
import com.cpe.springboot.user.model.User;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		return users;
	}

	public User getUser(Integer id) {
		return userRepository.findOne(id);
	}

	public User findByLoginAndPasswd(RequestUser requestUser) {
		return userRepository.findByLoginAndPasswd(requestUser.getLogin(), requestUser.getPasswd());
	}

	public void addUser(RequestUser reqUser) {
		User user = new User();
		user.setPasswd(reqUser.getPasswd());
		user.setLogin(reqUser.getLogin());
		userRepository.save(user);
	}

	public void updateUser(User user) {
		userRepository.save(user);

	}

	public void deleteUser(String id) {
		userRepository.delete(Integer.valueOf(id));
	}

}
