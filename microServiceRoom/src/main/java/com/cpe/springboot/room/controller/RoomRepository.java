package com.cpe.springboot.room.controller;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.room.model.Room;

public interface RoomRepository extends CrudRepository<Room, Integer> {
	
	public Room findById(String id);
	
	public Room findByName(String name);

}
