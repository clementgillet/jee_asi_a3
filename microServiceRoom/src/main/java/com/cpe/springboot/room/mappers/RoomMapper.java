package com.cpe.springboot.room.mappers;

import org.springframework.beans.BeanUtils;

import com.cpe.springboot.room.model.Room;
import com.cpe.springboot.room.model.RoomDto;

public class RoomMapper {
	
	public static RoomDto map(Room room) {
		RoomDto rDto = new RoomDto();
		BeanUtils.copyProperties(room, rDto);
		return rDto;
	}
}
