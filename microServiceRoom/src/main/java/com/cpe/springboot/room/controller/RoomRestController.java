package com.cpe.springboot.room.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.room.controller.RoomService;
import com.cpe.springboot.room.model.Room;

@RestController
public class RoomRestController {
	
	@Autowired
	private RoomService roomService;
	
	@RequestMapping("/rooms")
	private List<Room> getAllRooms() {
		System.out.println("LOG RoomRestController : getAllRooms() - GET /rooms");
		return roomService.getAllRooms();
	}
	
	@RequestMapping("/rooms/{id}")
	private Room getRoom(@PathVariable Integer id) {
		System.out.println("LOG RoomRestController : getRoom() - GET /rooms/" + id);
		return roomService.getRoom(id);

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/rooms")
	public Integer addRoom(@RequestBody Room room) {
		System.out.println("LOG RoomRestController : addRoom() - POST /rooms");
		return roomService.addRoom(room);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/rooms")
	public void updateRoom(@RequestBody Room room) {
		System.out.println("LOG RoomRestController : updateRoom() - PUT /rooms");
		roomService.updateRoom(room);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/rooms/addPlayer")
	public void addPlayerInRoom(@RequestBody Room room) {
		System.out.println("LOG RoomRestController : addPlayerInRoom() - PUT /rooms/addPlayer");
		roomService.addPlayer(room);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/rooms/{id}")
	public void deleteRoom(@PathVariable String id) {
		System.out.println("LOG RoomRestController : deleteRoom() - DELETE /rooms/" + id);
		roomService.deleteRoom(id);
	}
	

}
