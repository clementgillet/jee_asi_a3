package com.cpe.springboot.room.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.room.model.Room;

@Service
public class RoomService {

	@Autowired
	private RoomRepository roomRepository;

	public List<Room> getAllRooms() {
		List<Room> rooms = new ArrayList<>();
		roomRepository.findAll().forEach(rooms::add);
		return rooms;
	}

	public Room getRoom(Integer id) {
		return roomRepository.findOne(id);
	}

	public Integer addRoom(Room room) {
		//Vérifie l'objet
		if(null != room.getplayer1() && 
				null != room.getBet() &&
				null != room.getName()) {
			return roomRepository.save(room).getId();
		} else {
			//Sinon on return une erreur
			return null;
		}
	}
	
	public void updateRoom(Room room) {
		Room roomSaved = roomRepository.findOne(room.getId());
		//Parse all characteristics of room to detect changes
		if (room.getBet() != 0)
			roomSaved.setBet(room.getBet());
		if (room.getName() != "")
			roomSaved.setName(room.getName());
		if(room.getplayer1() == roomSaved.getplayer1()) {
			roomSaved.setCardPlayer1(room.getCardPlayer1());
		} else if (room.getplayer1() == roomSaved.getplayer2()) {
			roomSaved.setCardPlayer2(room.getCardPlayer1());
		}
		roomRepository.save(roomSaved);
	}
	
	public void addPlayer(Room room) {
		Room roomSaved = roomRepository.findOne(room.getId());
		//Parse all characteristics of room to detect changes
		roomSaved.setplayer2(room.getplayer2());
		roomRepository.save(roomSaved);
	}

	public void deleteRoom(String id) {
		roomRepository.delete(Integer.valueOf(id));
	}

}
