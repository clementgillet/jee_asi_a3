package com.cpe.springboot;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cpe.springboot.room.controller.RoomRepository;
import com.cpe.springboot.room.controller.RoomService;
import com.cpe.springboot.room.model.Room;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomServiceTests {
	
	@Autowired
	RoomService roomService;
	
	@Test
	public void addRoomTest()
	{
		RoomRepository roomRepository = Mockito.mock(RoomRepository.class);
		Room room = new Room(1,1,2,3,3,4,"room1");
		
		Mockito.when(roomRepository.save(room)).thenReturn(room);
		Assert.assertEquals(true, roomService.addRoom(room));
	}

	@Test
	public void addRoomFalseTest()
	{
		RoomRepository roomRepository = Mockito.mock(RoomRepository.class);
		Room room = new Room(null,null,null,null,null,null,"room1");
		
		Mockito.when(roomRepository.save(room)).thenReturn(room);
		Assert.assertEquals(false, roomService.addRoom(room));
	}
}
