package com.cpe.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class RestJpaMysqlSpringBootSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestJpaMysqlSpringBootSampleApplication.class, args);
	}
}
