var cards = [];

$(document).ready(function() {

	$.ajax({
		// Type de la requête
		type : "GET",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/market/" + user.id,
		dataType : 'json',
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		success : function(result) {
			console.dir(result);
			result.forEach(function(c) {
				addCardToList(c);
			});
			fillCurrentCard(result[0]);
			cards = result;
			console.log(result);
		},
		error : function(result) {
			console.log("Failed");
			console.dir(result);
		}
	});

});

function fillCurrentCard(card) {
	if (card != null){
		// FILL THE CURRENT CARD
		$('#cardFamilyImgId')[0].src = card.imageUrlFamily;
		$('#cardFamilyNameId')[0].innerText = card.familyName;
		$('#cardImgId')[0].src = card.imgUrl;
		$('#cardNameId')[0].innerText = card.name;
		$('#cardDescriptionId')[0].innerText = card.description;
		$('#cardHPId')[0].innerText = card.hp + " HP";
		$('#cardEnergyId')[0].innerText = card.energy + " Energy";
		$('#cardAttackId')[0].innerText = card.attack + " Attack";
		$('#cardDefenceId')[0].innerText = card.defence + " Defence";
		$('#cardPriceId')[0].innerText = card.price + " $";
	}
};

var IdButton = 0;
var selected_card = 0;
function selectCard(idButton) {
	fillCurrentCard(cards[idButton]);
	selected_card = cards[idButton].id;
};

function sellCard() {
	$.ajax({
		// Type de la requête
		type : "POST",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/market/sell/" + user.id + "/"
		+ selected_card,
		dataType : 'json',
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		success : function(result) {
			location.reload(true);
		},
		error : function(result) {
			location.reload(true);
		}
	});
}

function addCardToList(card) {

	content = "\
		<td> \
		<img  class='ui avatar image' src='"
		+ card.imgUrl
		+ "'> <span>"
		+ card.name
		+ " </span> \
		</td> \
		<td>"
		+ card.description
		+ "</td> \
		<td>"
		+ card.familyName
		+ "</td> \
		<td>"
		+ card.hp
		+ "</td> \
		<td>"
		+ card.energy
		+ "</td> \
		<td>"
		+ card.attack
		+ "</td> \
		<td>"
		+ card.defence
		+ "</td> \
		<td>"
		+ card.price
		+ "$</td>\
		<td>\
		<div id='sellButton_"
		+ IdButton
		+ "' onclick='selectCard("
		+ IdButton
		+ ")' class='ui vertical animated button' tabindex='0'>\
		<div class='hidden content'>Sell</div>\
		<div class='visible content'>\
		<i class='shop icon'></i>\
		</div>\
		</div>\
		</td>";

	$('#cardListId tr:last').after('<tr>' + content + '</tr>');
	IdButton++;
};