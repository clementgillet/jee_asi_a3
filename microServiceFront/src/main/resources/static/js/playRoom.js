$(document).ready(function(){

	$.ajax({
		// Type de la requête
		type : "GET",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/rooms/" + user.roomId,
		dataType : 'json',
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		success : function(result) {
			console.dir(result);
			getCard(result.cardPlayer1, 'user1');
			getCard(result.cardPlayer2, 'user2');
			startGame(result);
		},
		error : function(result) {
			console.log("Failed");
			console.dir(result);
		}
	});

});

function getCard(cardId, user){
	$.ajax({
		// Type de la requête
		type : "GET",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/cards/" + cardId,
		dataType : 'json',
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		success : function(result) {
			fillCard(user, result);
		},
		error : function(result) {
			return result;
		}
	});	
};

function startGame(room){

	//Create the game
	$.ajax({
		// Type de la requête
		type : "POST",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/games",
		dataType : 'json',
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		data : JSON.stringify(room),
		success : function(result) {
			launchGame(result);
		},
		error : function(result) {
			setTimeout(function(){
				$.ajax({
					// Type de la requête
					type : "GET",
					// Endpoint utilisé du Web Service
					url : "http://localhost:8080/games/getGameByRoom/" + room.id,
					dataType : 'json',
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json'
					},
					success : function(result) {
						launchGame(result);
					}
				});}, 5000);
		}
	});	
};

function launchGame(gameId){

	console.dir(gameId);

	//Create the game
	$.ajax({
		// Type de la requête
		type : "GET",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/games/play/" + gameId,
		dataType : 'json',
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		success : function(result) {
			setTimeout(function(){
				if (user.id == result){
					window.location.href = "http://localhost:8080/winGame.html";
				} else {
					window.location.href = "http://localhost:8080/loseGame.html";
				};}, 5000);
		},
		error : function(result) {
			console.dir(result);
		}
	});	
};


function fillCard(prefix, card){
	//FILL THE CURRENT CARD
	$('#'+prefix+'cardFamilyImgId')[0].src=card.imageUrlFamily;
	$('#'+prefix+'cardFamilyNameId')[0].innerText=card.familyName;
	$('#'+prefix+'cardImgId')[0].src=card.imgUrl;
	$('#'+prefix+'cardNameId')[0].innerText=card.name;
	$('#'+prefix+'cardDescriptionId')[0].innerText=card.description;
	$('#'+prefix+'cardHPId')[0].innerText=card.hp+" HP";
	$('#'+prefix+'cardEnergyId')[0].innerText=card.energy+" Energy";
	$('#'+prefix+'cardAttackId')[0].innerText=card.attack+" Attack";
	$('#'+prefix+'cardDefenceId')[0].innerText=card.defence+" Defence";

};


