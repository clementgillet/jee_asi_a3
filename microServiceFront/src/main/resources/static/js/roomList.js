$(document ).ready(function(){
    
	$.ajax({
		// Type de la requête
		type : "GET",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/rooms",
		dataType : 'json',
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		success : function(result) {
			console.dir(result);
			result.forEach(function(c) {
				addRoomToList(c.id, c.name, c.player1, c.bet);
			});
		},
		error : function(result) {
			console.log("Failed");
			console.dir(result);
		}
	});
    
     $("#createRoomButtonId").click(function(){
    	 window.location.href = "http://localhost:8080/roomSample.html";
    }); 
    
});


function addRoomToList(id, name, user, bet){
    
    content="<td> "+name+" </td> \
                            <td> "+user+" </td> \
                            <td> "+bet+" $</td> \
                            <td> \
                                <div class='center aligned'> \
                                    <div class='ui  vertical animated button' tabindex='0' onClick='onRoomSelected("+id+")'> \
                                        <div class='hidden content'>Play</div> \
                                        <div class='visible content'> \
                                            <i class='play circle icon'></i> \
                                        </div> \
                                    </div> \
                                </div> \
                            </td>";
    
    $('#roomListId tr:last').after('<tr>'+content+'</tr>');
    
    
};

function onRoomSelected(id){
	var room = {};
	room.id = id;
	room.player2 = user.id;
	
	
	$.ajax({
		// Type de la requête
		type : "PUT",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/rooms/addPlayer",
		dataType : "json",
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		data : JSON.stringify(room),
		complete : function(result) {
			console.dir(result);
			setCookie("roomId", id, 2);
			window.location.href = "http://localhost:8080/selectCardForPlay.html";
		}
	});
}