var user = {};

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

$(document).ready(function() {
	try_connect_user();
});

// fonction qui récupère les informations
// d'un utilisateur dans ses cookies
function try_connect_user() {
	console.dir(user);
	user.login = getCookie("login");
	user.id = getCookie("id");
	user.money = getCookie("money");
	user.roomId = getCookie("roomId");

	if (user.login != "") {
		$("#userNameId")[0].innerText = user.login;
		$("#money")[0].innerText = user.money;
		$("#roomNameId")[0].innerText = user.roomId;
	}
}


// essaye de connecter l'utilisateur en envoyant la requête
function connexion() {
	var conn_info = {};
	conn_info.login = $("#field_login").val();
	conn_info.passwd = $("#field_passwd").val();

	$.ajax({
		// Type de la requête
		type : "POST",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/conn",
		dataType : 'json',
		data : JSON.stringify(conn_info),
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		success : function(result) {
			setCookie("login", result.login, 2);
			setCookie("id", result.id, 2);
			setCookie("money", result.money, 2);
			window.location.href = "http://localhost:8080/cardHome.html";
		},
		error : function(result) {
			console.dir(result);
			alert("Erreur de connexion");
		}
	});
}

function submit() {
	console.log("Ici");
	window.location.href = "http://localhost:8080/formSample.html";
}
