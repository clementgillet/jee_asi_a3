$(document ).ready(function(){

	//Get all cards
	$.ajax({
		//Type de la requête
		type : "GET",
		//Endpoint utilisé du Web Service
		url : "http://localhost:8080/cards",
		headers : {'Accept':'application/json',
			'Content-Type':'application/json'},
			dataType : 'json',
			success : function(result) {
				fillCurrentCard(result[0]);
				console.dir(result);
			},
			error : function(result) {
				console.dir(result);
			}
	});

	//fillCurrentCard("https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/DC_Comics_logo.png/280px-DC_Comics_logo.png","DC comics","http://pngimg.com/uploads/superman/superman_PNG75.png","SUPERMAN","The origin story of Superman relates that he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton's destruction. Discovered and adopted by a farm couple from Kansas, the child is raised as Clark Kent and imbued with a strong moral compass. Early in his childhood, he displays various superhuman abilities, which, upon reaching maturity, he resolves to use for the benefit of humanity through a 'Superman' identity.",50,100,17,8);

	//Action to do when clicking on the research button
	$("#searchButtonId").click(function(){
		//Send request to service with the search ID
		$.ajax({
			//Type de la requête
			type : "GET",
			//Endpoint utilisé du Web Service
			url : "http://localhost:8080/cards/" + $("#searchId").val(),
			dataType : 'json',
			headers : {'Accept':'application/json',
				'Content-Type':'application/json'},
				success : function(result) {
					console.log("Réussie");
					console.dir(result);
					fillCurrentCard(result);
				},
				error : function(result) {
					console.log("Failed");
					console.dir(result);
				}
		});
	});



	//Action to do when buying a card
	$("#buyButton").click(function(){
		//Send request to service with the card
		$.ajax({
			//Type de la requête
			type : "POST",
			//Endpoint utilisé du Web Service
			url : "http://localhost:8080/market/buy/" + user.id + "/" + $("#searchId").val(),
			headers : {'Accept':'application/json',
				'Content-Type':'application/json'},
				dataType : 'json',
				success : function(result) {

					console.log("Réussie");
					console.dir(result);
				},
				error : function(result) {
					console.log("Failed");
					console.dir(result);
				}
		});
	});

});


function fillCurrentCard(card){
	//FILL THE CURRENT CARD
	$('#cardFamilyImgId')[0].src=card.imageUrlFamily;
	$('#cardFamilyNameId')[0].innerText=card.familyName;
	$('#cardImgId')[0].src=card.imgUrl;
	$('#cardNameId')[0].innerText=card.name;
	$('#cardDescriptionId')[0].innerText=card.description;
	$('#cardHPId')[0].innerText=card.hp+" HP";
	$('#cardEnergyId')[0].innerText=card.energy+" Energy";
	$('#cardAttackId')[0].innerText=card.attack+" Attack";
	$('#cardDefenceId')[0].innerText=card.defence+" Defence";
};



