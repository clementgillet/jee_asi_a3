$(document ).ready(function(){

	function checkRoom(){
		$.ajax({
			// Type de la requête
			type : "GET",
			// Endpoint utilisé du Web Service
			url : "http://localhost:8080/rooms/" + user.roomId,
			dataType : 'json',
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			success : function(result) {
				if(result.player2 != 0){
					window.location.href = "http://localhost:8080/selectCardForPlay.html";
				}
			},
			error : function(result) {
				console.log("Echec");
			}
		});
	}

	setInterval(checkRoom, 5000);

});
