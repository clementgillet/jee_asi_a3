$(document ).ready(function(){
	$("#submitButton").click(function(){

		//Get room datas
		var roomInfos = {
				player1: user.id,
				name: document.getElementById("formulaire").elements[0].value,
				bet: document.getElementById("formulaire").elements[1].value
		};

		//Send request to service with the name and bet
		$.ajax({
			//Type de la requête
			type : "POST",
			//Endpoint utilisé du Web Service
			url : "http://localhost:8080/rooms",
			headers : {'Accept':'application/json',
				'Content-Type':'application/json'},
				data : JSON.stringify(roomInfos),
				dataType : 'json',
				success : function(result) {
					setCookie("roomId", result, 2);
					window.location.href = "http://localhost:8080/waitPlayer.html";
					console.dir(result);
				},
				error : function(result) {
					setCookie("roomId", result, 2);
					window.location.href = "http://localhost:8080/waitPlayer.html";
					console.dir(result);
				}
		});
	});

	$("#cancelButton").click(function(){
		window.location.href = "http://localhost:8080/roomList.html";
	});

});
