var cards = [];
var selectedCard;

$(document ).ready(function(){

	$.ajax({
		// Type de la requête
		type : "GET",
		// Endpoint utilisé du Web Service
		url : "http://localhost:8080/market/" + user.id,
		dataType : 'json',
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		success : function(result) {
			console.dir(result);
			result.forEach(function(c) {
				addCardToList(c);
			});
			fillCurrentCard(result[0]);
			cards = result;
			console.log(result);
		},
		error : function(result) {
			console.log("Failed");
			console.dir(result);
		}
	});

	$("#playButtonId").click(function(){
		
		var room = {};
		room.id = user.roomId;
		room.cardPlayer1 = selectedCard.id;
		room.player1 = user.id;
		
		$.ajax({
			// Type de la requête
			type : "PUT",
			// Endpoint utilisé du Web Service
			url : "http://localhost:8080/rooms",
			dataType : "json",
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			data : JSON.stringify(room),
			complete : function(result) {
				console.dir(result);
				window.location.href = "http://localhost:8080/playRoom.html";
			}
		});
	});


});




function fillCurrentCard(card){
	//FILL THE CURRENT CARD
	$('#cardFamilyImgId')[0].src=card.imageUrlFamily;
	$('#cardFamilyNameId')[0].innerText=card.familyName;
	$('#cardImgId')[0].src=card.imgUrl;
	$('#cardNameId')[0].innerText=card.name;
	$('#cardDescriptionId')[0].innerText=card.description;
	$('#cardHPId')[0].innerText=card.hp+" HP";
	$('#cardEnergyId')[0].innerText=card.energy+" Energy";
	$('#cardAttackId')[0].innerText=card.attack+" Attack";
	$('#cardDefenceId')[0].innerText=card.defence+" Defence";

};


function addCardToList(card){

	content="\
		<td> \
		<img  class='ui avatar image' src='"+card.imgUrl+"'> <span>"+card.name+" </span> \
		</td> \
		<td>"+card.description+"</td> \
		<td>"+card.familyName+"</td> \
		<td>"+card.hp+"</td> \
		<td>"+card.energy+"</td> \
		<td>"+card.attack+"</td> \
		<td>"+card.defence+"</td> \
		<td>\
		<div class='ui vertical animated button' tabindex='0' onClick='onCardSelected("+card.id+")'>\
		<div class='hidden content'>Select</div>\
		<div class='visible content'>\
		<i class='checkmark icon'></i>\
		</div>\
		</div>\
		</td>";

	$('#cardListId tr:last').after('<tr>'+content+'</tr>');


};


function onCardSelected(id){
	cards.forEach(function(element) {
		if (element.id == id){
			selectedCard = element;
			return fillCurrentCard(element);
		}	
	});

}