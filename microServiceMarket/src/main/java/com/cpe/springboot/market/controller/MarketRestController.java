package com.cpe.springboot.market.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.card.model.CardDto;
import com.cpe.springboot.market.model.Market;

@RestController
public class MarketRestController {

	@Autowired
	private MarketService marketService;

	// récupère toutes les associations card/user
	@RequestMapping("/market")
	private List<Market> getAllCards() {
		System.out.println("LOG MarketRestController : getAllCards() - GET /market");
		return marketService.getAllCardsUsers();
	}

	// récupère les cards pour un utilisateur dont l'id est donné
	@RequestMapping("/market/{id}")
	private List<CardDto> getCard(@PathVariable Integer id) {
		System.out.println("LOG MarketRestController : getCard() - GET /market/" + id);
		return marketService.getCardUsers(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/market")
	public void updateCard(@RequestBody Market cardUsers) {
		System.out.println("LOG MarketRestController : updateCard() - PUT /market");
		marketService.updateCardUsers(cardUsers);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/market/buy/{idUser}/{idCard}")
	public void addCard(@PathVariable Integer idUser, @PathVariable Integer idCard) {
		System.out.println("LOG MarketRestController : addCard() - POST /market/buy/" + idUser + "/" + idCard);
		marketService.addCardUsers(idUser, idCard);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/market/sell/{idUser}/{idCard}")
	public void deleteCard(@PathVariable Integer idUser, @PathVariable Integer idCard) {
		System.out.println("LOG MarketRestController : deleteCard() - POST /market/sell/" + idUser + "/" + idCard);
		marketService.deleteCardUsers(idUser, idCard);
	}

}
