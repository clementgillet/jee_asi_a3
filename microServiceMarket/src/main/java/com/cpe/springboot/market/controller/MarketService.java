package com.cpe.springboot.market.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cpe.springboot.card.WSInterface.WSInterfaceCard;
import com.cpe.springboot.card.model.CardDto;
import com.cpe.springboot.market.model.Market;
import com.cpe.springboot.user.WSInterface.WSInterfaceUsers;
import com.cpe.springboot.user.model.UserDto;

@Service
public class MarketService {

	@Autowired
	MarketRepository marketRepository;
	
	@Autowired
	WSInterfaceUsers wsInterfaceUsers;
	
	@Autowired
	WSInterfaceCard wsInterfaceCard;

	public List<Market> getAllCardsUsers() {
		List<Market> cardUsers = new ArrayList<>();
		marketRepository.findAll().forEach(cardUsers::add);
		return cardUsers;
	}

	public List<CardDto> getCardUsers(Integer id) {
		List<Market> markets = marketRepository.findByIdUser(id);
		List<CardDto> cards = new ArrayList<>();
		for (Market m: markets) {
			cards.add(wsInterfaceCard.getCard(m.getIdCard()));
		}
		return cards;
	}

	public boolean addCardUsers(Integer idUser, Integer idCard) {
		UserDto user = wsInterfaceUsers.getUser(idUser);
		CardDto card = wsInterfaceCard.getCard(idCard);
		if (user.getMoney() >= card.getPrice()) {
			user.setMoney(user.getMoney() - card.getPrice());
			// On construit le nouvel objet
			// On place le premier argument à null car l'on ne connait pas son id
			Market cardUsers = new Market(null, user.getId(), card.getId());
			marketRepository.save(cardUsers);
			// On met à jour l'argent que possède l'utilisateur
			wsInterfaceUsers.setUserMoney(user);
		} else {
			return false;
		}
		return true;
	}

	public void updateCardUsers(Market cardUsers) {
		marketRepository.save(cardUsers);

	}

	public void deleteCardUsers(Integer idUser, Integer idCard) {
		System.out.println("Running user query");
		UserDto user = wsInterfaceUsers.getUser(idUser);
		System.out.println("user id " + user.getId());

		CardDto card = wsInterfaceCard.getCard(idCard);
		System.out.println("card id " + card.getId());

		user.setMoney(user.getMoney() + card.getPrice());
		marketRepository.deleteByIdUserAndIdCard(user.getId(), card.getId());
		// On met à jour l'argent que possède l'utilisateur
		wsInterfaceUsers.setUserMoney(user);
	}

}
