package com.cpe.springboot.market.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Market {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCardUsers;
	private Integer idUser;
	private Integer idCard;

	public Market(Integer idCardUsers, Integer idUser, Integer idCard) {
		super();
		this.idCardUsers = idCardUsers;
		this.idUser = idUser;
		this.idCard = idCard;
	}

	public Market() {
		this.idCardUsers = null;
		this.idUser = null;
		this.idCard = null;
	}

	public Integer getId() {
		return idCardUsers;
	}

	public void setId(Integer id) {
		this.idCardUsers = id;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Integer getIdCard() {
		return idCard;
	}

	public void setIdCard(Integer idCard) {
		this.idCard = idCard;
	}

}
