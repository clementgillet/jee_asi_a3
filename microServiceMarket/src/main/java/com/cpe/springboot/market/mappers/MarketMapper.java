package com.cpe.springboot.market.mappers;

import org.springframework.beans.BeanUtils;

import com.cpe.springboot.market.dto.MarketDto;
import com.cpe.springboot.market.model.Market;

public class MarketMapper {
	
	public static MarketDto map(Market market) {
		MarketDto mDto = new MarketDto();
		BeanUtils.copyProperties(market, mDto);
		return mDto;
	}
}
