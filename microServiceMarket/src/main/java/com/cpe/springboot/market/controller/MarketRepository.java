package com.cpe.springboot.market.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.market.model.Market;

public interface MarketRepository extends CrudRepository<Market, Integer> {

	public List<Market> findByIdUser(Integer idUser);

	public void deleteByIdUserAndIdCard(Integer idUser, Integer idCard);

}
