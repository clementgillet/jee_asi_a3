package com.cpe.springboot;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cpe.springboot.card.WSInterface.WSInterfaceCard;
import com.cpe.springboot.card.model.CardDto;
import com.cpe.springboot.market.controller.MarketRepository;
import com.cpe.springboot.market.controller.MarketService;
import com.cpe.springboot.market.model.Market;
import com.cpe.springboot.user.WSInterface.WSInterfaceUsers;
import com.cpe.springboot.user.model.UserDto;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MarketServiceTests {
	
	@InjectMocks
	MarketService marketService;
	
	@Mock
	WSInterfaceUsers wsInterfaceUsers;
	
	@Mock
	WSInterfaceCard wsInterfaceCard;
	
	@Mock
	MarketRepository marketRepository;
	
	@Test
	public void addCardUsersTest() {
		UserDto user = new UserDto(1, "test", "test", 200);
		CardDto card = new CardDto(1, "test", "test", "test", 
				"test", 100, 100, 100, 100, "test");
		
		//On mock l'utilisateur retourné
		Mockito.when(wsInterfaceUsers.getUser(
				Mockito.anyInt())).thenReturn(user);
		
		//On mock la carte retounée
		Mockito.when(wsInterfaceCard.getCard(
				Mockito.anyInt())).thenReturn(card);
		
		//On mock la sauvegarde de l'utilisateur
		Mockito.when(marketRepository.save(
				Mockito.any(Market.class))).thenReturn(new Market());

		Mockito.doNothing().when(wsInterfaceUsers).setUserMoney(
				Mockito.any(UserDto.class));
		
		Assert.assertEquals(true, marketService.addCardUsers(1, 1));
	}
	
	@Test
	public void addCardUsersErrorTest() {
		UserDto user = new UserDto(1, "test", "test", 50);
		CardDto card = new CardDto(1, "test", "test", "test", 
				"test", 100, 100, 100, 100, "test");
		
		//On mock l'utilisateur retourné
		Mockito.when(wsInterfaceUsers.getUser(
				Mockito.anyInt())).thenReturn(user);
		
		//On mock la carte retounée
		Mockito.when(wsInterfaceCard.getCard(
				Mockito.anyInt())).thenReturn(card);
		
		//On mock la l'achat de la carte
		Mockito.when(marketRepository.save(
				Mockito.any(Market.class))).thenReturn(new Market());
		
		//On ne fait rien quand l'on enregistre l'utilisateur
		Mockito.doNothing().when(wsInterfaceUsers).setUserMoney(
				Mockito.any(UserDto.class));
		
		Assert.assertEquals(false, marketService.addCardUsers(1, 1));
	}
	
	@Test
	public void deleteCardUsersTest() {
		UserDto user = new UserDto(1, "test", "test", 50);
		CardDto card = new CardDto(1, "test", "test", "test", 
				"test", 100, 100, 100, 100, "test");
		
		//On mock l'utilisateur retourné
		Mockito.when(wsInterfaceUsers.getUser(
				Mockito.anyInt())).thenReturn(user);
		
		//On mock la carte retounée
		Mockito.when(wsInterfaceCard.getCard(
				Mockito.anyInt())).thenReturn(card);
		
		//On mock la sauvegarde de l'utilisateur
		Mockito.when(marketRepository.save(
				Mockito.any(Market.class))).thenReturn(new Market());

		//On ne fait rien quand on supprime une carte de l'utilisateur
		Mockito.doNothing().when(marketRepository).deleteByIdUserAndIdCard(
				Mockito.anyInt(), Mockito.anyInt());
		
		//On ne fait rien quand l'on enregistre l'utilisateur
		Mockito.doNothing().when(wsInterfaceUsers).setUserMoney(
				Mockito.any(UserDto.class));
		
		this.marketService.deleteCardUsers(1, 1);
		
		Assert.assertEquals((int)150, (int)user.getMoney());		
	}

}
