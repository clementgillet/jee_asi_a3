package com.cpe.springboot.card.mappers;

import org.springframework.beans.BeanUtils;

import com.cpe.springboot.card.model.Card;
import com.cpe.springboot.card.model.CardDto;

public class CardMapper {

	public static CardDto map(Card market) {
		CardDto mDto = new CardDto();
		BeanUtils.copyProperties(market, mDto);
		return mDto;
	}

	public static Card map(CardDto cDto) {
		Card c = new Card();
		BeanUtils.copyProperties(cDto, c);
		return c;
	}

}
