package com.cpe.springboot.card.controller;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.card.model.Card;

public interface CardRepository extends CrudRepository<Card, Integer> {

	public Card findById(Integer id);

}
