package com.cpe.springboot.card.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.card.controller.CardService;
import com.cpe.springboot.card.mappers.CardMapper;
import com.cpe.springboot.card.model.Card;
import com.cpe.springboot.card.model.CardDto;

@RestController
public class CardRestController {

	@Autowired
	private CardService cardService;

	@RequestMapping("/cards")
	private List<Card> getAllCards() {
		System.out.println("LOG CardRestController : getAllCards() - GET /cards");
		return cardService.getAllCards();
	}

	@RequestMapping("/cards/{id}")
	private Card getCard(@PathVariable Integer id) {
		System.out.println("LOG CardRestController : getCard() - GET /cards/" + id);
		return cardService.getCard(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cards")
	public void addCard(@RequestBody CardDto card) {
		System.out.println("LOG CardRestController : addCard() - POST /cards");
		cardService.addCard(CardMapper.map(card));
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/cards")
	public void updateCard(@RequestBody CardDto card) {
		System.out.println("LOG CardRestController : updateCard() - PUT /cards");
		cardService.updateCard(CardMapper.map(card));
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/cards/{id}")
	public void deleteCard(@PathVariable String id) {
		System.out.println("LOG CardRestController : deleteCard() - DELETE /cards/" + id);
		cardService.deleteCard(id);
	}

}
