package com.cpe.springboot.card.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.model.Card;
import com.cpe.springboot.card.model.CardDto;

@Service
public class CardService {

	@Autowired
	private CardRepository cardRepository;

	public List<Card> getAllCards() {
		List<Card> cards = new ArrayList<>();
		cardRepository.findAll().forEach(cards::add);
		return cards;
	}

	public Card getCard(Integer id) {
		return cardRepository.findById(id);
	}

	public void addCard(Card card) {
		cardRepository.save(card);
	}

	public void updateCard(Card card) {
		cardRepository.save(card);

	}

	public void deleteCard(String id) {
		cardRepository.delete(Integer.valueOf(id));
	}

}
