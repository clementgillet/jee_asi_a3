# Projet CPE JEE ASI

* Clément Gillet
* Julien Laurencin 
* Olivier Pinon
* Loic Ousset

# Activités

- Développement du back-end du projet suivant l'architecture microservices
- Développement du front-end du projet avec JQuery
- Mise en place de tests unitaires avec JUnit + Mockito
- Mise en place intégration continue avec Gitlab CI + Sonarqube


# Vidéo YouTube de démo du projet

(lien)

