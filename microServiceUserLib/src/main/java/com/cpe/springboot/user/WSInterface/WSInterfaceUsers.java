package com.cpe.springboot.user.WSInterface;

import java.net.URI;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cpe.springboot.user.model.UserDto;

@Service
public class WSInterfaceUsers {

	public UserDto getUser(Integer userId) {

		// Crée une requête
		final String uri = "http://localhost:8085/users/" + userId;

		// Crée un RestTemplate pour parser le résultat de la requête
		RestTemplate restTemplate = new RestTemplate();

		// Envoie une requête et déserialize le résultat en un UserDto
		return restTemplate.getForObject(uri, UserDto.class);
	}

	public void setUserMoney(UserDto uDto) {
		final String url = "http://localhost:8085/users/money/" + uDto.getId() + "/" + uDto.getMoney();

		try {
			//TODO remove comment
			System.out.println("Modification de l'argents");
			URI uri = new URI(url);
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.put(uri, null);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void updateUser(UserDto uDto) {
		final String url = "http://localhost:8085/users/";
		try {
			URI uri = new URI(url);
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.put(uri, uDto);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
