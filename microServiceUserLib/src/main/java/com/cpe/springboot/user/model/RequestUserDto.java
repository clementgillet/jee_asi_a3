package com.cpe.springboot.user.model;

public class RequestUserDto {

	private String login;
	private String passwd;

	public RequestUserDto(String login, String passwd) {
		super();
		this.login = login;
		this.passwd = passwd;
	}

	public RequestUserDto() {
		this.login = "";
		this.passwd = "";
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

}
