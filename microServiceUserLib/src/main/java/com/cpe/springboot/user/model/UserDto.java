package com.cpe.springboot.user.model;

public class UserDto {

	private Integer id;
	private String login;
	private String passwd;
	private Integer money;

	public UserDto(Integer id, String login, String passwd, Integer money) {
		super();
		this.id = id;
		this.login = login;
		this.passwd = passwd;
		this.money = money;
	}

	public UserDto() {
		this.id = null;
		this.login = "";
		this.passwd = "";
		this.money = 100;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public Integer getMoney() {
		return money;
	}

	public void setMoney(Integer money) {
		this.money = money;
	}

}
