package com.cpe.springboot.card.WSInterface;

import java.net.URI;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cpe.springboot.card.model.CardDto;

@Service
public class WSInterfaceCard {

	public CardDto getCard(Integer cardId) {
		// Crée une requête
		final String uri = "http://localhost:8081/cards/" + cardId;

		// Crée un RestTemplate pour parser le résultat de la requête
		RestTemplate restTemplate = new RestTemplate();

		// Envoie une requête et déserialize le résultat en une CardDto
		return restTemplate.getForObject(uri, CardDto.class);
	}

	public void updateCard(CardDto cardDto) {
		final String url = "http://localhost:8081/cards/";
		try {
			URI uri = new URI(url);
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.put(uri, cardDto);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
