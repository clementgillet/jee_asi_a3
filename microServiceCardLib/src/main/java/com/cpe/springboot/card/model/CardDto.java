package com.cpe.springboot.card.model;

public class CardDto {

	public CardDto() {
		this.id = 0;
		this.family = "";
		this.imgurl = "";
		this.name = "";
		this.description = "";
		this.hp = 0;
		this.energy = 0;
		this.attack = 0;
		this.defence = 0;
		this.imageUrlFamily = "";
		this.price = 100;
	}

	public CardDto(Integer id, String family, String imgurl, String name, String description, Integer hp,
			Integer energy, Integer attack, Integer defence, String imageUrlFamily) {
		super();
		this.id = id;
		this.family = family;
		this.imgurl = imgurl;
		this.name = name;
		this.description = description;
		this.hp = hp;
		this.energy = energy;
		this.attack = attack;
		this.defence = defence;
		this.imageUrlFamily = imageUrlFamily;
		this.price = 100;
	}

	private Integer id;
	private String family;
	private String imgurl;
	private String name;
	private String description;
	private Integer hp;
	private Integer energy;
	private Integer attack;
	private Integer defence;
	private String imageUrlFamily;
	private Integer price;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFamilyName() {
		return family;
	}

	public void setFamilyName(String familyName) {
		this.family = familyName;
	}

	public String getImgUrl() {
		return imgurl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgurl = imgUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getHp() {
		return hp;
	}

	public void setHp(Integer hp) {
		this.hp = hp;
	}

	public Integer getEnergy() {
		return energy;
	}

	public void setEnergy(Integer energy) {
		this.energy = energy;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getDefence() {
		return defence;
	}

	public void setDefence(Integer defence) {
		this.defence = defence;
	}

	public String getImageUrlFamily() {
		return imageUrlFamily;
	}

	public void setImageUrlFamily(String imageUrlFamily) {
		this.imageUrlFamily = imageUrlFamily;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

}
