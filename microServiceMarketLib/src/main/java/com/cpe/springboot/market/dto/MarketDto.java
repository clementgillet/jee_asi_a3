package com.cpe.springboot.market.dto;

public class MarketDto {

	private Integer idCardUsers;
	private Integer idUser;
	private String idCard;

	public MarketDto(Integer idCardUsers, Integer idUser, String idCard) {
		super();
		this.idCardUsers = idCardUsers;
		this.idUser = idUser;
		this.idCard = idCard;
	}

	public MarketDto() {
		this.idCardUsers = null;
		this.idUser = null;
		this.idCard = "";
	}

	public Integer getId() {
		return idCardUsers;
	}

	public void setId(Integer id) {
		this.idCardUsers = id;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

}
